# Installation of KYPO CRP base network

`apt-get install jq` or `dnf install jq`

`pipenv shell`

`pipenv sync`

`source <application-credentials.sh>` (not part of this repo)

`source openstack-defaults.sh` (here, you can override default OpenStack flavors and images)

`openstack network list --external` (list external networks and pick one)

`openstack security group rule list default` (find `<rule>` id for `icmp|IPv4|0.0.0.0/0`)

`openstack security group rule delete <rule>`

`./bootstrap.sh <external_network_name>`

`./create-base.sh`

`./ansible-check-base.sh`

`./ansible-user-access.sh`
