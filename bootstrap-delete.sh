#!/usr/bin/env bash

while true; do
  read -p "Are you sure (y/n)?" yn
  case $yn in
  [Yy]*)
    break
    ;;
  [Nn]*)
    exit
    ;;
  *)
    echo "Please answer yes or no."
    ;;
  esac
done


KYPO_PROJECT_ID=$(openstack floating ip list --format yaml | yq -r .[0].Project)
KYPO_PROJECT_NAME=$(openstack project show --format yaml "${KYPO_PROJECT_ID}" | yq -r .name)
KYPO_KEYFILE="${KYPO_PROJECT_NAME}_kypo-base-key.key"
KYPO_USER_KEYFILE="${KYPO_PROJECT_NAME}_kypo-user-key.key"

READ_YAML=$(cat kypo-base-params.yml)

KEYNAME=$(echo "${READ_YAML}" | yq -r .parameters.kypo.kypo_base_head_keypair_name)
echo "Deleting keypair ${KEYNAME}."
openstack keypair delete "${KEYNAME}" || exit 1

HEAD_FIP_ID=$(echo "${READ_YAML}" | yq -r .parameters.kypo.kypo_base_head_floating_ip_id)
echo "Deleting floating ip ${HEAD_FIP_ID}."
openstack floating ip delete "${HEAD_FIP_ID}" || exit 1

PROXY_FIP_ID=$(echo "${READ_YAML}" | yq -r .parameters.kypo.kypo_base_proxy_floating_ip_id)
echo "Deleting floating ip ${PROXY_FIP_ID}."
openstack floating ip delete "${PROXY_FIP_ID}" || exit 1

echo "Deleting secrets and configuration."
rm "${KYPO_KEYFILE}"
rm "${KYPO_USER_KEYFILE}"
rm kypo-base-params.yml
rm provisioning/ansible_hosts.yml
